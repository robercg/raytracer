
public class Triangle {
	private Vector v0;
	private Vector v1;
	private Vector v2;

	public Triangle(Vector v0, Vector v1, Vector v2) {
		this.v0 = v0;
		this.v1 = v1;
		this.v2 = v2;
		
	}
	
	public Vector getv0() { 
		return v0;
	}
	
	public Vector getv1() {
		return v1;
	}
	
	public Vector getv2() {
		return v2;
	}
	
	
	/*public boolean intersect(Ray ray) {
		boolean planeHit = plane.intersect(ray);
		if(!planeHit) return false;

		double uu, uv, vv, wu, wv, D;
		uu = u.dot(u);
		uv = u.dot(v);
		vv = v.dot(v);

		Vector w = new Vector(ray.pointAt(plane.getT(ray)).sub(v0));

		wu = w.dot(u);
		wv = w.dot(v);
		D = uv * uv  - uu * vv;

		double s, t;
		s = (uv * wv - vv * wu) / D;
		if(s < 0 || s > 1) return false;
		t = (uv * wu - uu * wv) / D;
		if(t < 0 || (s + t) > 1) return false;

		return true;
	}*/
	public boolean intersect(Ray r){
		Vector v0v1 = new Vector(v0,v1);
		Vector v0v2 = new Vector(v0,v2);
		Vector pvec = r.getDirection().norm().Cross(v0v2);
		double det = v0v1.dot(pvec);
		if (det < 0.00000000001) return false;
		if (Math.abs(det) < 0.00000000001) return false;
		double invDet = 1.0 / det;
		Vector tvec = new Vector(v0,r.getOrigin());
		double u = tvec.dot(pvec) * invDet;
		 if (u < 0 || u > 1) return false; 
		 
		    Vector qvec = tvec.Cross(v0v1); 
		    double v = r.getDirection().norm().dot(qvec) * invDet;
		    if (v < 0 || (u + v) > 1) return false; 
		 
		    double t = v0v2.dot(qvec) * invDet; 
		    return true; 
	}
		
}
