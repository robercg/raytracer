
public class Plane {
	private final double a, b, c, d;
	private final Vector normal;

	public Plane(double a, double b, double c, double d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		double t;
		this.normal = new Vector(a, b, c).norm();
	}

	public boolean intersect(Ray ray) {
		// from http://www.tar.hu/gamealgorithms/ch22lev1sec2.html
		double denominator = (a * ray.getDirection().getX() + b * ray.getDirection().getY() + c * ray.getDirection().getZ());
		if(denominator == 0.0) return false;

		double t = - (a * ray.getOrigin().getX() + b * ray.getOrigin().getY() + c * ray.getOrigin().getZ() + d) / denominator;

		if(t < 0) return false;

		return true;
	}
	
	public double getT(Ray ray) {
		double denominator = (a * ray.getDirection().getX() + b * ray.getDirection().getY() + c * ray.getDirection().getZ());

		double t = - (a * ray.getOrigin().getX() + b * ray.getOrigin().getY() + c * ray.getOrigin().getZ() + d) / denominator;

		return t;
	}
}