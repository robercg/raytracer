
public class Vector {
	private double x;
	private double y;
	private double z;
	
	public Vector(double a, double b, double c) {
		this.x = a;
		this.y = b;
		this.z = c;
	}
	
	public Vector(Vector a) {
		this.x = a.x;
		this.y = a.y;
		this.z = a.z;
	}
	
	public Vector(Vector a, Vector b) {
		this.x = b.getX()-a.getX();
		this.y = b.getY()-a.getY();
		this.z = b.getZ()-a.getZ();
	}
	
	
	public void set(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public static Vector add (Vector a, Vector b) {
		return new Vector((a.getX() + b.getX()), (a.getY() + b.getY()), (a.getZ() + b.getZ()));
	}
	
	public Vector add (Vector b) {
		return new Vector((x + b.getX()), (y + b.getY()), (z + b.getZ()));
	}
	
	public static Vector sub (Vector a, Vector b) {
		return new Vector((a.getX() - b.getX()), (a.getY() - b.getY()), (a.getZ() - b.getZ()));
	}
	
	public Vector sub (Vector b) {
		return new Vector((x - b.getX()), (y - b.getY()), (z - b.getZ()));
	}
	
	public Vector dot(double b) {
		return new Vector(x*b, y*b, z*b);
	}
	
	public double dot(Vector b) {
		return (x * b.getX()) + (y * b.getY()) + (z*b.getZ()); 
	}
	
	public Vector norm () {
		double norm = Math.sqrt(x*x + y*y + z*z);
		return new Vector(x/norm, y/norm, z/norm);
	}
	
	public double length() {
		return Math.sqrt(x*x + y*y + z*z);
	}
	
	public Vector Cross (Vector  a) {
		return new Vector((y*a.z - z*a.y),(z*a.x -x*a.z),(x*a.y-y*a.x));
	}
}
