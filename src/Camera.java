
public class Camera {
	private Vector u;
	private Vector v;
	private Vector w;
	private Vector right;
	private Vector llc;
	private Vector horizontal;
	private Vector vertical;
	private Vector origin;
	private double height, width;
	
	public Camera(Vector origin, Vector target, Vector vup, double fov, double aspectRatio) {
		double theta = fov * Math.PI/180;
		this.origin = origin;
		double half_height = Math.tan(theta/2);
		double half_width = aspectRatio * half_height;
		w = origin.sub(target).norm();
		u = vup.Cross(w).norm();
		v = w.Cross(u);
		llc = origin.sub(u.dot(half_width)).sub(v.dot(half_height)).sub(w);
		horizontal = u.dot(half_width*2);
		vertical = v.dot(half_height*2);
		height = Math.tan(fov);
		width = height * aspectRatio;
	}
	
	public Ray makeRay(double s, double t) {
		Vector temp = llc.add(horizontal.dot(s)).add(vertical.dot(t)).sub(origin);
		Ray ray = new Ray(origin, temp);
		return ray;
	}
	
}
