public class TriangleIntersection {
	public static boolean rayTriangleIntersect( 
		    Vector orig, Vector dir, 
		    Triangle t1, 
		    double t) 
		{ 
			Vector v0 = t1.getv0();
			Vector v1 = t1.getv1();
			Vector v2 = t1.getv2();
		    // compute plane's normal
		    Vector v0v1 = v1.sub(v0); 
		    Vector v0v2 = v2.sub(v0); 
		    // no need to normalize
		    Vector N = v0v1.Cross(v0v2); // N 
		    float area2 = (float) N.length(); 
		 
		    // Step 1: finding P
		 
		    // check if ray and plane are parallel ?
		    double NdotRayDirection = N.dot(dir); 
		    if (Math.abs(NdotRayDirection) < 0.0000000000000001) // almost 0 
		        return false; // they are parallel so they don't intersect ! 
		 
		    // compute d parameter using equation 2
		    double d = N.dot(v0); 
		 
		    // compute t (equation 3)
		    t = ((N.dot(orig) + d) / NdotRayDirection); 
		    // check if the triangle is in behind the ray
		    if (t < 0) return false; // the triangle is behind 
		 
		    // compute the intersection point using equation 1
		    Vector P = orig.add(dir.dot(t)); 
		 
		    // Step 2: inside-outside test
		    Vector C; // vector perpendicular to triangle's plane 
		 
		    // edge 0
		    Vector edge0 = v1.sub(v0); 
		    Vector vp0 = P.sub(v0); 
		    C = edge0.Cross(vp0); 
		    if (N.dot(C) < 0) return false; // P is on the right side 
		 
		    // edge 1
		    Vector edge1 = v2.sub(v1); 
		    Vector vp1 = P.sub(v1); 
		    C = edge1.Cross(vp1); 
		    if (N.dot(C) < 0)  return false; // P is on the right side 
		 
		    // edge 2
		    Vector edge2 = v0.sub(v2); 
		    Vector vp2 = P.sub(v2); 
		    C = edge2.Cross(vp2); 
		    if (N.dot(C) < 0) return false; // P is on the right side; 
		 
		    return true; // this ray hits the triangle 
		} 
}
/*
    private static double EPSILON = 0.0000001;

    public static boolean rayIntersectsTriangle(Vector rayOrigin, 
    											Vector rayVector,
                                                Triangle inTriangle,
                                                Vector outIntersectionPoint) {
    	Vector vertex0 = inTriangle.getVertex0();
    	Vector vertex1 = inTriangle.getVertex1();
    	Vector vertex2 = inTriangle.getVertex2();
    	Vector edge1 = new Vector(0,0,0);
    	Vector edge2 = new Vector(0,0,0);
    	Vector h = new Vector(0,0,0);
    	Vector s = new Vector(0,0,0);
    	Vector q = new Vector(0,0,0);
        double a, f, u, v;
        edge1.sub(vertex1, vertex0);
        edge2.sub(vertex2, vertex0);
        h = rayVector.Cross(edge2);
        a = edge1.dot(h);
        if (a > -EPSILON && a < EPSILON) {
            return false;    // This ray is parallel to this triangle.
        }
        f = 1.0 / a;
        s.sub(rayOrigin, vertex0);
        u = f * (s.dot(h));
        if (u < 0.0 || u > 1.0) {
            return false;
        }
        q = s.Cross(edge1);
        v = f * rayVector.dot(q);
        if (v < 0.0 || u + v > 1.0) {
            return false;
        }
        // At this stage we can compute t to find out where the intersection point is on the line.
        double t = f * edge2.dot(q);
        if (t > EPSILON && t < 1/EPSILON) // ray intersection
        {
            outIntersectionPoint.set(0.0, 0.0, 0.0);
            Vector temp = rayVector.dot(t).add(rayOrigin);
            outIntersectionPoint = temp;
            return true;
        } else // This means that there is a line intersection but not a ray intersection.
        {
            return false;
        }
    }
}*/