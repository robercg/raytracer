

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;


public class Raytracer {

	private static ArrayList<Triangle> triangles = ReadShapeFile.readFile("bunny.txt");

	public static Vector colorear(Ray r) {
		Vector center = new Vector(0,0,0);
		
		for(int i = 0; i < triangles.size(); i++) {
			Triangle t = triangles.get(i);
			/*if (TriangleIntersection.rayTriangleIntersect(r.getOrigin(), r.getDirection(), t, 230)){
				Vector vec = new Vector(1,1,0);
				return  vec;
			} */
			if (t.intersect(r)) {
				Vector v1 = t.getv0();
				Vector v2 = t.getv1();
				Vector v3 = t.getv2();
				Vector z1 = new Vector(v1,v2);
				Vector z2 = new Vector(v1,v3);
				Vector n = z1.Cross(z2).norm();
				/*Vector vec = new Vector(1,1,0);
				return  vec;*/
				Vector n2 = new Vector ((n.getX()+1), (n.getY()+1), (n.getZ()+1));
				return n2.dot(0.5);
			}

		}

		double t = hitsphere(center, 0.5, r);
		if (t > 0.0) {
			Vector n = (r.pointAt(t).sub(center)).norm();
			Vector n2 = new Vector ((n.getX()+1), (n.getY()+1), (n.getZ()+1));
			return n2.dot(0.5);
		}
			
		Vector direction = r.getDirection().norm();

		t = 0.5*(direction.getY() + 1.0);
		Vector uno = new Vector(1.0,1.0,1.0);
		Vector dos = new Vector(0.5,0.7,1.0);
		return Vector.add(uno.dot(1.0-t), dos.dot(t));
	}
	
	public static double hitsphere(Vector center, double radius, Ray r) {
		Vector oc = new Vector(Vector.sub(r.getOrigin(), center));
		double a = r.getDirection().dot(r.getDirection());
		double b = 2.0 * r.getDirection().dot(oc);
		double c = oc.dot(oc) - radius*radius;
		double discriminant = b*b -4*a*c;
		if (discriminant < 0) {
			return -1;
		} else {
			return ((-b -Math.sqrt(discriminant))/(2.0*a));
		}
	}

	
	public static final void main(String[] args) {
        File f = null; 

        
		int width = 300;
		int height = 200;

		Vector lookfrom = new Vector(-2, 0, 0);
		Vector lookat = new Vector(4, 0, 0);
		Vector lookup = new Vector(0, 0, 1);
		double fov = 80;
		double aspectRatio = width /height;
		Random rand = new Random();
		double[] jitterMatrix = {
			    -1.0/4.0,  3.0/4.0,
			     1.0/4.0, -3.0/4.0,
			};

		BufferedImage resultImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Camera c = new Camera(lookfrom, lookat, lookup, fov, aspectRatio);
		for (int y = height-1; y  >= 0 ; y--) {
			for (int x = 0; x < width; x++) {
				double tempX = 0;
				double tempY = 0;
				double tempZ = 0;
				for (int sample = 0; sample < 1; ++sample) {
					double u = ((double) y +  jitterMatrix[2*sample]) / (double) height;
					double v = ((double) x +  jitterMatrix[2*sample+1]) / (double) width;
					Ray r = c.makeRay(u, v);
					
					Vector col = colorear(r);
					tempX = col.getX() +tempX;
					tempY = col.getY() +tempY;
					tempZ = col.getZ() +tempZ;
				
				} 	
				Color temp = new Color((int)((tempX/2) * 255),(int)((tempY/2) * 255),(int)((tempZ/2)*255));
				resultImage.setRGB(x, y, temp.getRGB());
			
			}	System.out.println(y);
		}
		
		try
        { 
            f = new File("Resultado9.png");
            ImageIO.write(resultImage, "png", f); 
        } 
        catch(IOException e) 
        { 
            System.out.println(e); 
        } 
	}

}
