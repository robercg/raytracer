
/**
 * This class reads a shape file.  For the format of this shape file, see the assignment description.
 * Also, please see the shape files ExampleShapes.txt, ExampleShapesStill.txt, and TwoRedCircles.txt
 *
 * @author you
 *
 */

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadShapeFile {


	/**
	 * Reads the shape data if the shape is a polygon
	 * @param in the scanner of the file
	 * @return the polygon object to be added to the queue
	 */
	private static ArrayList<Triangle> readDataFile(Scanner in) {

		ArrayList<Triangle> triangles = new ArrayList<Triangle>();
		ArrayList<Vector> vertex = new ArrayList<Vector>();

		while(in.hasNext()){
		
			String e = in.next();
				if (e.equals("v")) {
					double v1 = Double.parseDouble(in.next());
			        double v2 = Double.parseDouble(in.next());
			        double v3 = Double.parseDouble(in.next());
			        System.out.println(v3);
			        Vector temp = new Vector(v1*3,v2*3,v3*3);
			        vertex.add(temp);
						
				} else if (e.equals("f")) {
					int f1 = Integer.parseInt(in.next());
			        int f2 = Integer.parseInt(in.next());
			        int f3 = Integer.parseInt(in.next());
					Vector v1x = vertex.get(f1-1);
					Vector v2x = vertex.get(f2-1);
					Vector v3x = vertex.get(f3-1);
					Triangle t = new Triangle(v1x,v2x,v3x);
					triangles.add(t);
				} 

				}



			if (in.hasNextLine()) {
				in.nextLine();
			}

        return triangles;
	}
	

	/**
	 * Method to read the file and return a queue of shapes from this file. The
	 * program should handle the file not found exception here and shut down the
	 * program gracefully.
	 * 
	 * @param filename
	 *            the name of the file
	 * @return the queue of shapes from the file
	 */
	public static ArrayList<Triangle> readFile(String filename) {
		Scanner in = null; 
		try {
			in = new Scanner (new File(filename));
		} catch (FileNotFoundException e) {
			System.out.println ("Cannot open file");
			System.exit(0);
		}
		return ReadShapeFile.readDataFile(in);  
	}
}
