
public class Ray {
	private Vector origin;
	private Vector direction;
	
	public Ray(Vector origin, Vector direction) {
		this.origin = origin;
		this.direction = direction;
	}
	
	public void setDirection (Vector direction) {
		this.direction = direction;
	}
	
	public void setOrigin (Vector origin) {
		this.origin = origin;
	}
	
	public Vector getOrigin() { 
		return origin;
	}
	
	public Vector getDirection() {
		return direction;
	}
	
	public Vector pointAt(double t) {
		Vector newD = (direction.dot(t));
		Vector res = newD.add(origin);
		return res; 
	}
	
	
	
}
